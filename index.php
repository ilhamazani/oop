<?php
require_once ('Ape.php');
require_once ('Frog.php');

$sheep = new Animal("Shaun");
echo $sheep->name; // Shaun
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // False


$sungokong = new Ape("kera sakti");
echo "<br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "<br>";
$kodok->jump() ; // "hop hop"

?>